﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace ListViewExample
{
    public partial class ListViewUsingList : ContentPage
    {
        List<int> listOfInt = new List<int>();
    
        public ListViewUsingList()
        {
            InitializeComponent();

            PopulateListView();
        }

        private void PopulateListView()
        {
            for (int i = 0; i < 8; i++)
            {
                listOfInt.Add(i);
            }

            BrokenList.ItemsSource = listOfInt;
        }

        void Handle_Clicked(object sender, System.EventArgs e)
        {
            listOfInt.Add(listOfInt.Count);
        }
    }
}
