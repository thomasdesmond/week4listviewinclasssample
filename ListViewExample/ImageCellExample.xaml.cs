﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace ListViewExample
{
    public partial class ImageCellExample : ContentPage
    {
        public ImageCellExample()
        {
            InitializeComponent();

            PopulateImageCellListView();
        }

        private void PopulateImageCellListView()
        {
            var imageCellOne = new ImageCell
            {
                ImageSource = "google.png",
                Text = "Google",
                Detail = "Google.com",
            };

            var imageCellTwo = new ImageCell();
            imageCellTwo.ImageSource = "twitter.png";
            imageCellTwo.Text = "Twitter";
            imageCellTwo.Detail = "http://www.twitter.com";

            ObservableCollection<ImageCell> imageCellCollection = new ObservableCollection<ImageCell>();
            imageCellCollection.Add(imageCellOne);
            imageCellCollection.Add(imageCellTwo);

            ImageCellListView.ItemsSource = imageCellCollection;
        }

        void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            ListView listView = sender as ListView;
            ImageCell imageCellTapped = (ImageCell)listView.SelectedItem;

            Device.OpenUri(new Uri(imageCellTapped.Detail));
        }
    }
}
