﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ListViewExample
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        async void Handle_NavigateToListViewUsingList(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new ListViewUsingList());
        }

        async void Handle_NavigateToListViewUsingObservableCollection(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new ListViewWithObservableCollection());
        }
        
        async void Handle_NavigateToListViewUsingImageCell(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new ImageCellExample());
        }
    }
}
