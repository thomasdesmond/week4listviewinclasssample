﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace ListViewExample
{
    public partial class ListViewWithObservableCollection : ContentPage
    {
        ObservableCollection<int> listOfInt = new ObservableCollection<int>();
    
        public ListViewWithObservableCollection()
        {
            InitializeComponent();

            PopulateListView();
        }

        private void PopulateListView()
        {
            for (int i = 0; i < 8; i++)
            {
                listOfInt.Add(i);
            }

            WorkingList.ItemsSource = listOfInt;
        }

        void Handle_Clicked(object sender, System.EventArgs e)
        {
            listOfInt.Add(listOfInt.Count);
        }
    }
}
